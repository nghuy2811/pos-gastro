export const constants = {
  FORMAT_DATE_FOR_API: "YYYY-MM-DD",
  FORMAT_DATE_FOR_UI: "dd-MM-yyyy",
  FORMAT_DATE_TIME_FOR_UI: "DD-MM-YYYY HH:MM:ss",
};
